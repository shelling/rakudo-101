#!/usr/bin/env perl6

role Hello {
    method foo() {
        "foo in Hello";
    }
}

class Foo does Hello {
    has Str $.foo is readonly;
    has Str $.public is rw;
    has Str $private;

    method set_private($v) {
        $!private = $v;
    }

    method get_private {
        return $!private;
    }

    method set_public($v) {
        $!public = $v;
    }

    method get_public {
        return $!public;
    }
}

say Foo.new(foo => "hello world").foo;

my $foo = Foo.new;

$foo.set_private("here");
say $foo.get_private;

$foo.set_public("another");
say $foo.get_public;

$foo.public = "another accessor";
say $foo.public;


say $foo.foo;
say $foo.does(Hello);
