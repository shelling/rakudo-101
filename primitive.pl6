#!/usr/bin/env perl6

use v6;

my $parsel = (hello => "world", shelling => "ford");
say $parsel.WHAT;
say $parsel.^methods;

my @array = $parsel.list;
say @array.perl;
say @array.WHAT;
say @array[0];
say @array[0].WHAT;
say @array>>.keys;
say @array[0].keys;
say @array.keys;
say @array.values;

my %hash = $parsel.list;
say %hash.perl;
say %hash.WHAT;
say %hash.keys;
say %hash>>.keys;
