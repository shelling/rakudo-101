#!/usr/bin/env perl6

module Yahoo {
	our sub hello {
		"hello, i am yahoo";
	}
}

say Yahoo::hello;
