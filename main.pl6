#!/usr/bin/env perl6
use v6;

multi sub MAIN("install", $package, Bool :$verbose, Bool :$force) {
    say $package;
    if $verbose { say "verbose: $verbose" } else { say "verbose: { $verbose.defined }" }
    if $force   { say "force:   $force"   } else { say "force: { $verbose.defined }"   }
}

multi sub MAIN("list", *@args) {
    say @args;
}
