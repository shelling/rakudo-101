#!/usr/bin/env perl6

class A {
    has $.a is rw;
}

class B {
    has $.b is rw;
}

class Foo is A is B {
    our $.count = 1;
    has $.number is rw;

    our $test;
    method ^test {
        return $.count;
    }

}

my $foo1 = Foo.new;
my $foo2 = Foo.new;
say $foo1.count;
say $foo2.count;

$foo1.count = 10;
say $foo1.count;
say $foo2.count;

$foo2.count = 100;
say $foo1.count;
say $foo2.count;

$foo1.number = 1;
$foo2.number = 2;

say $foo1.number;
say $foo2.number;

$foo1.number = 3;
$foo2.number = 4;

say $foo1.number;
say $foo2.number;

$foo1.a = "A1";
$foo2.a = "A2";
$foo1.b = "B1";
$foo2.b = "B2";

say $foo1.perl;
say $foo2.perl;

my $C = class { also is A; };
my $D = class { also is B; };
my $c1 = $C.new; say $c1.perl;
my $d1 = $D.new; say $d1.perl;

class M {
    has $.meta is rw;
    method test {
        self.meta;
    }
}

my $m1 = M.new(meta => "meta");
say $m1.perl;
say $m1.test;
