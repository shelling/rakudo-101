#!/usr/bin/env perl6

# vim: ft=perl6

use v6;

for 1..10 <-> $i {
    say $i;
}

role Blue {
    method blue {
        say "blue";
    }
}

class Foo does Blue {
    has Num $!num;
    has Str $.foo is rw;

    method hello(Str $name) {
        say "hello, $name. I am $.foo";
    }

    method some(&block) {
        block("foo");
    }
}

sub some(&block) {
    block("foo");
}

my $f = Foo.new(foo => "first foo");

$f.hello("shelling");

$f.some(-> $arg { say "some block with $arg" });

some -> $arg { say "feed block with $arg" };

say Foo.blue;


say "this is a bit of code { [1,2,3,4].perl }";
say <hello world shelling>.perl;
say "interpolate object { <hello world shelling>.perl }";
say "hash {hello => 'world'}";

