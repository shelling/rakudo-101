#!/usr/bin/env perl6

class Player {
    has $.status is rw = "stop";

    our sub info {
        "perl6 player";
    }

    method play {
        $.status = "playing";
        "play";
    }
}

my $p = Player.new;

say Player::info;

say $p.status;

$p.play;

say $p.status;

