#!/usr/bin/env perl6

use v6;

sub square-by-copy( $a is copy ) {
    $a = $a * $a;
    return $a
}

sub square-by-rw( $a is rw ) {
    $a = $a * $a;
    return $a;
}

my $int = 10;

say square-by-copy($int);  # 100
say $int;                  # 10

say square-by-rw($int);    # 100
say $int;                  # 100

my @list = 1..3;

for @list <-> $e {
  say $e + 1;
}

for @list <-> $e {
  $e++;
}

say @list; # 2 3 4

